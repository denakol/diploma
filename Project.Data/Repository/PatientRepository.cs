﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Project.Data.Infrastructure;
using Project.Data.IRepository;
using Project.Model.Models;

namespace Project.Data.Repository
{
    public class PatientRepository : RepositoryBase<ApplicationUser>,IPatientRepository
    {
        public PatientRepository(IDatabaseFactory databaseFactory) : base(databaseFactory)
        {
        }

        public IEnumerable<ApplicationUser> GetByIdDoctor(string id)
        {
            var doctor = DataContext.Users.FirstOrDefault(x => x.Id == id);
            if (doctor == null)
            {
                return new List<ApplicationUser>();
            }
            var ids = doctor.Patients.Select(x => x.PatientId).ToList();

            return DataContext.Users.Where(x => ids.Contains(x.Id));
            
           
        }



        public override IEnumerable<ApplicationUser> GetAll()
        {
            return DataContext.Users.Where(
                x => x.Roles.Select(y => y.RoleId).Contains(DataContext.Roles.FirstOrDefault(z => z.Name == "Patient").Id)).ToList();
        }
    }
}
