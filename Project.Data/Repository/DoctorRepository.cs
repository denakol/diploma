﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Text;
using System.Threading.Tasks;
using Project.Data.IRepository;
using Project.Model.Models;
using Project.Data.Infrastructure;

namespace Project.Data.Repository
{
    public class DoctorRepository : RepositoryBase<ApplicationUser>, IDoctorRepository
    {
        public DoctorRepository(IDatabaseFactory databaseFactory)
            : base(databaseFactory)
        {

        }


        public ApplicationUser GetDoctorByPatientId(string id)
        {
            var patient = DataContext.Users.FirstOrDefault(x => x.Id == id);

            if (patient != null)
            {
                var firstOrDefault = patient.Doctor.FirstOrDefault();
                if (firstOrDefault != null) return firstOrDefault.Doctor;
            }
            return null;

        }

        public override ApplicationUser GetById(string id)
        {
            return DataContext.Users.Where(
                x =>
                    x.Roles.Select(y => y.RoleId).Contains(DataContext.Roles.FirstOrDefault(z => z.Name == "Doctor").Id))
                .FirstOrDefault(x => x.Id == id);
        }


        public override IEnumerable<ApplicationUser> GetAll()
        {
            return  DataContext.Users.Where(
                x => x.Roles.Select(y => y.RoleId).Contains(DataContext.Roles.FirstOrDefault(z => z.Name == "Doctor").Id)).ToList();
        }
    }
}
