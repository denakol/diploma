﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Project.Data.Infrastructure;
using Project.Data.IRepository;
using Project.Model.Models;

namespace Project.Data.Repository
{
    public class PatientInfoRepository : RepositoryBase<PatientInfo>, IPatientInfoRepository
    {
        public PatientInfoRepository(IDatabaseFactory databaseFactory)
            : base(databaseFactory)
        {


        }



        public override void Delete(PatientInfo entity)
        {
            var entities = entity.Diseases;
            DataContext.Diseases.RemoveRange(entities);

            base.Delete(entity);



        }
    }
}
