﻿using System.Data.Entity;
using Microsoft.AspNet.Identity.EntityFramework;
using Project.Model.Models;

namespace Project.Data
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
        }
        public virtual void Commit()
        {
            SaveChanges();
        }

     

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }


        public DbSet<Message> Messages { get; set; }
        public DbSet<Client> Clients { get; set; }
        public DbSet<RefreshToken> RefreshTokens { get; set; }

        public DbSet<DoctorInfo> DoctorInfos { get; set; }
        public DbSet<PatientInfo> PatientInfos { get; set; }

        public DbSet<Disease> Diseases { get; set; }

        public DbSet<GpsMessage> GpsMessages { get; set; }
        public DbSet<StandartMessage> StandartMessages { get; set; }
    }
}
