namespace Project.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Addinstruction : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Instructions",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Title = c.String(),
                        Description = c.String(),
                        StartDate = c.DateTime(nullable: false),
                        EndDate = c.DateTime(nullable: false),
                        PatientId = c.String(maxLength: 128),
                        DoctorId = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.DoctorInfoes", t => t.DoctorId)
                .ForeignKey("dbo.PatientInfoes", t => t.PatientId)
                .Index(t => t.PatientId)
                .Index(t => t.DoctorId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Instructions", "PatientId", "dbo.PatientInfoes");
            DropForeignKey("dbo.Instructions", "DoctorId", "dbo.DoctorInfoes");
            DropIndex("dbo.Instructions", new[] { "DoctorId" });
            DropIndex("dbo.Instructions", new[] { "PatientId" });
            DropTable("dbo.Instructions");
        }
    }
}
