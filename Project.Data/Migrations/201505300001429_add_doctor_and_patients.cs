namespace Project.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class add_doctor_and_patients : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.DoctorInfoes",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Education = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.Id)
                .Index(t => t.Id);
            
            CreateTable(
                "dbo.DoctorPatients",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        DoctorId = c.String(maxLength: 128),
                        PatientId = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.PatientId)
                .ForeignKey("dbo.AspNetUsers", t => t.DoctorId)
                .Index(t => t.DoctorId)
                .Index(t => t.PatientId);
            
            CreateTable(
                "dbo.PatientInfoes",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.Id)
                .Index(t => t.Id);
            
            CreateTable(
                "dbo.Diseases",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        UserInfo_Id = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.PatientInfoes", t => t.UserInfo_Id)
                .Index(t => t.UserInfo_Id);
            
            AddColumn("dbo.AspNetUsers", "FirstName", c => c.String());
            AddColumn("dbo.AspNetUsers", "LastName", c => c.String());
            AddColumn("dbo.AspNetUsers", "SemiName", c => c.String());
            AddColumn("dbo.AspNetUsers", "Brithday", c => c.DateTime(nullable: false));
            AddColumn("dbo.AspNetUsers", "DateRegister", c => c.DateTime(nullable: false));
            AddColumn("dbo.AspNetUsers", "About", c => c.String());
            AddColumn("dbo.AspNetUsers", "Address", c => c.String());
            AddColumn("dbo.AspNetUsers", "Href", c => c.String());
            AddColumn("dbo.AspNetUsers", "IsMale", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.DoctorInfoes", "Id", "dbo.AspNetUsers");
            DropForeignKey("dbo.DoctorPatients", "DoctorId", "dbo.AspNetUsers");
            DropForeignKey("dbo.PatientInfoes", "Id", "dbo.AspNetUsers");
            DropForeignKey("dbo.Diseases", "UserInfo_Id", "dbo.PatientInfoes");
            DropForeignKey("dbo.DoctorPatients", "PatientId", "dbo.AspNetUsers");
            DropIndex("dbo.Diseases", new[] { "UserInfo_Id" });
            DropIndex("dbo.PatientInfoes", new[] { "Id" });
            DropIndex("dbo.DoctorPatients", new[] { "PatientId" });
            DropIndex("dbo.DoctorPatients", new[] { "DoctorId" });
            DropIndex("dbo.DoctorInfoes", new[] { "Id" });
            DropColumn("dbo.AspNetUsers", "IsMale");
            DropColumn("dbo.AspNetUsers", "Href");
            DropColumn("dbo.AspNetUsers", "Address");
            DropColumn("dbo.AspNetUsers", "About");
            DropColumn("dbo.AspNetUsers", "DateRegister");
            DropColumn("dbo.AspNetUsers", "Brithday");
            DropColumn("dbo.AspNetUsers", "SemiName");
            DropColumn("dbo.AspNetUsers", "LastName");
            DropColumn("dbo.AspNetUsers", "FirstName");
            DropTable("dbo.Diseases");
            DropTable("dbo.PatientInfoes");
            DropTable("dbo.DoctorPatients");
            DropTable("dbo.DoctorInfoes");
        }
    }
}
