namespace Project.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class add_messages : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.GpsMessages",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Latitude = c.Double(nullable: false),
                        Longitude = c.Double(nullable: false),
                        Speed = c.Double(nullable: false),
                        DateTime = c.DateTime(nullable: false),
                        PatientId = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.PatientInfoes", t => t.PatientId)
                .Index(t => t.PatientId);
            
            CreateTable(
                "dbo.StandartMessages",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        PatientId = c.String(maxLength: 128),
                        HeartRate = c.Int(nullable: false),
                        DateTime = c.DateTime(nullable: false),
                        InternalData = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.PatientInfoes", t => t.PatientId)
                .Index(t => t.PatientId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.StandartMessages", "PatientId", "dbo.PatientInfoes");
            DropForeignKey("dbo.GpsMessages", "PatientId", "dbo.PatientInfoes");
            DropIndex("dbo.StandartMessages", new[] { "PatientId" });
            DropIndex("dbo.GpsMessages", new[] { "PatientId" });
            DropTable("dbo.StandartMessages");
            DropTable("dbo.GpsMessages");
        }
    }
}
