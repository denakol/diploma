using System;
using System.Data.Entity.Migrations;
using System.Linq;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Project.Model.Models;

namespace Project.Data.Migrations
{
    public sealed class Configuration : DbMigrationsConfiguration<ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        public void Seeder()
        {
            Seed(new ApplicationDbContext());
        }

        protected override void Seed(ApplicationDbContext context)
        {
            var manager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(new ApplicationDbContext()));
            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(new ApplicationDbContext()));

            var user = new ApplicationUser()
            {
                UserName = "AdminUser",
                Email = "admin@admin.com",
                EmailConfirmed = true,
                DateRegister = DateTime.Now,
                Brithday = DateTime.Now
            };
 
            manager.Create(user, "wUcheva$3a");

            if (!roleManager.Roles.Any())
            {
                roleManager.Create(new IdentityRole { Name = "Admin" });
                roleManager.Create(new IdentityRole { Name = "Patient" });
                roleManager.Create(new IdentityRole { Name = "Doctor" });
            }

            var adminUser = manager.FindByName("AdminUser");
            manager.AddToRoles(adminUser.Id, new string[] { "Admin"});

        }
    }
}
