﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Project.Model.Models;
using Project.Data.Infrastructure;
namespace Project.Data.IRepository
{
    public interface IInstructionRepository : IRepository<Instruction>
    {
    }
}
