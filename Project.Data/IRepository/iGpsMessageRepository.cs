﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Project.Data.Infrastructure;
using Project.Model.Models;

namespace Project.Data.IRepository
{
    public interface IGpsMessageRepository : IRepository<GpsMessage>
    {
    }
}
