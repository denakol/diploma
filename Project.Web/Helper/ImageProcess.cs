﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Web;
using Microsoft.AspNet.Identity;

namespace Project.Web.Helper
{
    public class ImageOperation
    {
           /// <summary>
        /// Resizes and saves the image
        /// </summary>
        /// <param name="savePath">Path where file will be save</param>
        /// <param name="fileName">Name of the file</param>
        /// <param name="imageBuffer">Stream</param>
        /// <param name="maxSideSize">Max size of image by side</param>
        /// <param name="isMakeItSquare">Make is square</param>
        public static void ResizeAndSave(String savePath, String fileName, Stream imageBuffer, Int32 maxSideSize, Boolean isMakeItSquare)
        
        {

            Int32 newWidth;
            Int32 newHeight;
            Image image = Image.FromStream(imageBuffer);
            CorrectImageRotation(image);
            Int32 oldWidth = image.Width;
            Int32 oldHeight = image.Height;
            Bitmap newImage;
            if (isMakeItSquare)
            {
                Int32 smallerSide = oldWidth >= oldHeight ? oldHeight : oldWidth;
                Double coeficient = maxSideSize / (Double)smallerSide;
                newWidth = Convert.ToInt32(coeficient * oldWidth);
                newHeight = Convert.ToInt32(coeficient * oldHeight);
                Bitmap tempImage = new Bitmap(image, newWidth, newHeight);
                Int32 cropX = (newWidth - maxSideSize) / 2;
                Int32 cropY = (newHeight - maxSideSize) / 2;
                newImage = new Bitmap(maxSideSize, maxSideSize);
                Graphics tempGraphic = Graphics.FromImage(newImage);
                tempGraphic.SmoothingMode = SmoothingMode.AntiAlias;
                tempGraphic.InterpolationMode = InterpolationMode.HighQualityBicubic;
                tempGraphic.PixelOffsetMode = PixelOffsetMode.HighQuality;
                tempGraphic.DrawImage(tempImage, new Rectangle(0, 0, maxSideSize, maxSideSize), cropX, cropY, maxSideSize, maxSideSize, GraphicsUnit.Pixel);
            }
            else
            {
                Int32 maxSide = oldWidth >= oldHeight ? oldWidth : oldHeight;

                if (maxSide > maxSideSize)
                {
                    Double coeficient = maxSideSize / (Double)maxSide;
                    newWidth = Convert.ToInt32(coeficient * oldWidth);
                    newHeight = Convert.ToInt32(coeficient * oldHeight);
                }
                else
                {
                    newWidth = oldWidth;
                    newHeight = oldHeight;
                }
                newImage = new Bitmap(image, newWidth, newHeight);
            }
            Image img = (Image)newImage;
            if (!Directory.Exists(savePath))
            {
                Directory.CreateDirectory(savePath);
            }
            img.Save(Path.ChangeExtension(Path.Combine(savePath, fileName), ".jpg"));
            image.Dispose();
            newImage.Dispose();
            img.Dispose();
        }

        /// <summary>
        /// Saves the image
        /// </summary>
        /// <param name="savePath">Path where file will be save</param>
        /// <param name="fileName">Name of the file</param>
        /// <param name="imageBuffer">Stream</param>
        /// <param name="fileExtension">Extension of file</param>
        public static void SaveOriginal(String savePath, String fileName, Stream imageBuffer, ImageFormat imageFormat)
        {
            if (!Directory.Exists(savePath))
            {
                Directory.CreateDirectory(savePath);
            }
            String fileExtension = GetExtension(imageFormat);
            var image = Image.FromStream(imageBuffer);
            CorrectImageRotation(image);
            image.Save(Path.ChangeExtension(Path.Combine(savePath, fileName), fileExtension), imageFormat);
        }

  

    /*    public static void CropAndSave(String savePath, String sourcePath, AvatarModel imageModel, Int32 resultHeight, Int32 resultWidth)
        {
            var newImage = new Bitmap(resultWidth, resultHeight);
            Graphics tempGraphic = Graphics.FromImage(newImage);
            tempGraphic.SmoothingMode = SmoothingMode.AntiAlias;
            tempGraphic.InterpolationMode = InterpolationMode.HighQualityBicubic;
            tempGraphic.PixelOffsetMode = PixelOffsetMode.HighQuality;
            Image image = Image.FromFile(sourcePath);            
            var destRect = new Rectangle(0, 0, resultWidth, resultHeight);
            tempGraphic.DrawImage(image, destRect, imageModel.Left, imageModel.Top, imageModel.Width, imageModel.Height, GraphicsUnit.Pixel);
            if (!Directory.Exists(savePath))
            {
                Directory.CreateDirectory(savePath);
            }
            newImage.Save(Path.Combine(savePath, imageModel.FileName), GetImageFormat(imageModel.FileName));
        }
        */
        /// <summary>
        /// Save image without crop
        /// </summary>
        /// <param name="savePath">Where to save</param>
        /// <param name="sourcePath">Where to get</param>
        /// <param name="filename">Filename</param>
        /// <param name="resultHeight">Image height</param>
        /// <param name="resultWidth">Image width</param>
        public static void SaveWithoutCrop(String savePath, String sourcePath, String filename, Int32 resultHeight, Int32 resultWidth)
        {
            Image image = Image.FromFile(sourcePath);
            if (!Directory.Exists(savePath))
            {
                Directory.CreateDirectory(savePath);
            }
            image.Save(Path.Combine(savePath, filename), GetImageFormat(filename));
        }

        /// <summary>
        /// Get file extension by image format
        /// </summary>
        /// <param name="format">Image format</param>
        /// <returns></returns>
        private static String GetExtension(ImageFormat format) 
        {
            if (format == ImageFormat.Jpeg)
                return ".jpg";
            if (format == ImageFormat.Png)
                return ".png";
            return String.Empty;
        }

        /// <summary>
        /// Get image format by file name
        /// </summary>
        /// <param name="fileName">File name</param>
        /// <returns></returns>
        private static ImageFormat GetImageFormat(String fileName)
        {
            if (Path.GetExtension(fileName).ToLower() == ".png")
                return ImageFormat.Png;
            return ImageFormat.Jpeg;
        }

        /// <summary>
       /// Turn image to correct orientation
        /// </summary>
        /// <param name="image">Image</param>
        private static void CorrectImageRotation(Image image)
        {
            Int32 orientation = 0;
            foreach (PropertyItem p in image.PropertyItems)
            {
                if (p.Id == ORIENTATION_PROPERTY_ID)
                {
                    orientation = (Int32)p.Value[0];
                    break;
                }
            }
            switch (orientation)
            {
                case 8:
                    if (image.Width > image.Height)
                    {
                        image.RotateFlip(RotateFlipType.Rotate270FlipNone);
                    }
                    break;
                case 3:
                        image.RotateFlip(RotateFlipType.Rotate180FlipNone);
                    break;
                case 6:
                    if (image.Width > image.Height)
                    {
                        image.RotateFlip(RotateFlipType.Rotate90FlipNone);
                    }
                    break;
            }
        }
        public const Int32 ORIENTATION_PROPERTY_ID = 274;
    }

    
    
}