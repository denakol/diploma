﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Microsoft.AspNet.Identity;
using Project.Service.DTO;
using Project.Service.IService;

namespace Project.Web.Controllers.Api
{



   // [Authorize]
    public class PatientController : ApiController
    {
        
         private readonly IPatientService _patientService;

         public PatientController(IPatientService patientService)
        {
            _patientService = patientService;
        }

       
        // GET: api/Patinets
        public IEnumerable<SummaryOfPatient> Get()
        {
            var roles = ((ClaimsIdentity)User.Identity).Claims
                            .Where(c => c.Type == ClaimTypes.Role)
                            .Select(c => c.Value);

            if (roles.Contains("Admin"))
            {
                return _patientService.GetAllPatients();
            }  
            return _patientService.GetPatientByDoctor(User.Identity.GetUserId());
        }

        // GET: api/Patinets/5
        public AddPatient Get(String id)
        {
            return _patientService.GetPatient(id);
        }



        [Route("api/patient/{id}/summary")]
        [HttpGet]
        public SummaryOfPatient GetSummary(String id)
        {
           
           return _patientService.GetSummaryPatient(id);
            
        }

        // POST: api/Patinets
        public async Task<IHttpActionResult> Post([FromBody]AddPatient patient)
        {
            await _patientService.CreatePatient(patient);
            return Ok(patient);

        }

        // PUT: api/Patinets/5
        public IHttpActionResult Put(String id, [FromBody]AddPatient value)
        {
            _patientService.UpdatePatient(value);
            return Ok(value);
        }

        // DELETE: api/Patinets/5
        public IHttpActionResult Delete(String id)
        {
            _patientService.RemovePatient(id);
            return Ok(id);
        }
    }
}
