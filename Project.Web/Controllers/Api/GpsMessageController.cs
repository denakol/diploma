﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Microsoft.AspNet.Identity;
using Project.Service.DTO;
using Project.Service.IService;

namespace Project.Web.Controllers.Api
{
    public class GpsMessageController : ApiController
    {

      private readonly IMessageService _messageService;

      public GpsMessageController(IMessageService messageService)
        {
            _messageService = messageService;
        }
        // GET: api/GpsMessage


                [Route("api/gpsMessage/{id}/{startDate:datetime?}/{endDate:datetime?}")]
          [HttpGet]
      public IEnumerable<GpsMessageDTO> Get(String id, DateTime? startDate = null, DateTime? endDate = null)
        {

            if (startDate == null && endDate == null)
            {
                return _messageService.GetGpsMessageForUserSummaryPage(id);
            }
            return null;
        }

        /*  public IEnumerable<GpsMessageDTO> Get(String id)
          {
              return _messageService.GetGpsMessageForUserSummaryPage(id);
          }*/



        public IHttpActionResult Post([FromBody]GpsMessageDTO value)
        {

            var id = User.Identity.GetUserId();
            if (!String.IsNullOrEmpty(id))
            {
                _messageService.CreateGpsMessage(id, value);
                return Ok(value);
            }
            else
            {
                return BadRequest();
            }
            return Ok(value);
        }

        // PUT: api/GpsMessage/5
        public IHttpActionResult Put(int id, [FromBody]GpsMessageDTO value)
        {
            return Ok(value);
        }

        // DELETE: api/GpsMessage/5
        public void Delete(int id)
        {
        }
    }
}
