﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Results;
using Microsoft.AspNet.Identity;
using Project.Service.DTO;
using Project.Service.IService;

namespace Project.Web.Controllers.Api
{
    public class StandartMessageController : ApiController
    {

  
         private readonly IMessageService _messageService;



         public StandartMessageController(IMessageService messageService)
        {
            _messageService = messageService;
        }

          [Route("api/StandartMessage/{id}/{startDate:datetime?}/{endDate:datetime?}")]
     
        // GET: api/StandartMessage
         public SummaryStandartMessage Get(String id, DateTime? startDate=null, DateTime? endDate=null)
        {
            if (startDate == null && endDate == null)
             {
                 return _messageService.GetStandartMessageForUserSummaryPage(id);
             }

             return null;
          //  return _messageService.GetAllStandartMessageForUser(userId);
        }

 

        // POST: api/StandartMessage
        public IHttpActionResult Post([FromBody]StandartMessageDTO value)
        {
            var id = User.Identity.GetUserId();
            if (!String.IsNullOrEmpty(id))
            {
                _messageService.CreateStandartMessage(id,value);
                return Ok(value);
            }
            else
            {
                return BadRequest();
            }
           
            
        }

        // PUT: api/StandartMessage/5
        public IHttpActionResult Put(int id, [FromBody]StandartMessageDTO value)
        {
            return Ok(value);

        }

        // DELETE: api/StandartMessage/5
        public void Delete(int id)
        {
        }
    }
}
