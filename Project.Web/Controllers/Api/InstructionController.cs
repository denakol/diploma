﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Microsoft.AspNet.Identity;
using Project.Service.DTO;
using Project.Service.IService;

namespace Project.Web.Controllers.Api
{
    public class InstructionController : ApiController
    {
        private readonly IInstructionService _instructionService;

        public InstructionController(IInstructionService messageService)
        {
            _instructionService = messageService;
        }
        // GET: api/GpsMessage



        [HttpGet]
        // GET: api/Instruction
        public IEnumerable<InstructionModel> Get(String id)
        {
            return _instructionService.GetByUser(id);
        }

        

        // POST: api/Instruction
        public void Post([FromBody]InstructionModel value)
        {

            value.DoctorId = User.Identity.GetUserId();
            _instructionService.CreateInstruction(value);

        }

        // PUT: api/Instruction/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Instruction/5
        public void Delete(int id)
        {
            _instructionService.Archive(id);
        }
    }
}
