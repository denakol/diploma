﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using Project.Service.DTO;
using Project.Service.IService;

namespace Project.Web.Controllers.Api
{
    public class DoctorAAController : ApiController
    {


        private readonly IDoctorService _doctorService;

        public DoctorAAController(IDoctorService doctorService)
        {
            _doctorService = doctorService;
        }

        // GET: api/Doctor
        public IEnumerable<SummaryOfDoctor> Get()
        {
            return _doctorService.GetAllDoctors();
        }

        // GET: api/Doctor/5
        public AddDoctor Get(String id)
        {
            return _doctorService.GetDoctor(id);
        }

        // POST: api/Doctor
        public IHttpActionResult Post([FromBody]AddDoctor doctor)
        {
            _doctorService.CreateDoctor(doctor);
            return Ok(doctor);
        }

        // PUT: api/Doctor/5
        public IHttpActionResult Put(String id, [FromBody]AddDoctor doctor)
        {
            _doctorService.UpdateDoctor(doctor);
            return Ok(doctor);
        }

        // DELETE: api/Doctor/5
        public IHttpActionResult Delete(String id)
        {
            _doctorService.RemoveDoctor(id);
            return Ok(id);
        }
    }
}
