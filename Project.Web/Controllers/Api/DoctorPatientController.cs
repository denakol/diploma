﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Project.Service.DTO;
using Project.Service.IService;

namespace Project.Web.Controllers.Api
{
    public class DoctorPatientController : ApiController
    {
        private readonly IDoctorPatientService _doctorPatientService;

        public DoctorPatientController(IDoctorPatientService doctorPatientService)
        {
            _doctorPatientService = doctorPatientService;
        }
        // GET: api/DoctorPatient
        public IEnumerable<DoctorPatientInfo> Get()
        {
            return _doctorPatientService.GetAll() ;
        }

        // GET: api/DoctorPatient/5
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/DoctorPatient
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/DoctorPatient/5
        public IHttpActionResult Put(string id, [FromBody]DoctorPatientInfo value)
        {
            _doctorPatientService.Update(value);
            return Ok(value);

        }

        // DELETE: api/DoctorPatient/5
        public void Delete(int id)
        {
        }
    }
}
