﻿using System;
using System.Collections.Generic;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Project.Web.Helper;

namespace Project.Web.Controllers
{
    public class ImageController : Controller
    {

        // GET: Image
        [HttpPost]
        public JsonResult Post()
        {
            HttpPostedFileBase imageUploaded = Request.Files[0];
            if (imageUploaded != null && imageUploaded.ContentLength > 0)
            {
                var resolution = Path.GetExtension(imageUploaded.FileName);
                if (IMAGE_EXTENTIONS.Contains(resolution.ToLower()))
                {
                    if (imageUploaded.ContentLength < MAX_SIZE_OF_FILE_AVATAR)
                    {
                        String fileName = Guid.NewGuid().ToString();


                        ImageOperation.ResizeAndSave(Server.MapPath("~") + "/images/", fileName, imageUploaded.InputStream,250,true );
                        return Json(new { imagePath = "images/" +Path.ChangeExtension(fileName, ".jpg") }, "text/plain");
                    }
                    return Json(new { error = "error"});
                }
                return Json(new { error = "error" });
            }
            return Json(new { error = "error" });
        }


        public static String[] IMAGE_EXTENTIONS = new String[] { ".png", ".jpg", ".jpeg" };
        public const Int32 MIN_IMAGE_SIZE = 250;
        public const Int32 MAX_IMAGE_SIZE = 900;
        public const Int32 MAX_SIZE_OF_FILE_AVATAR = 4194304;
        public const Int32 MAX_SIZE_OF_FILE = 10485760;
        public const Int32 MAX_FILES_COUNT = 40;
        public const Int32 DIGEST_DEFAULT_PORT = 25;
        public const Int32 MAX_AGE = 99;
        public const Int32 MIN_AGE = 18;
        public const Int32 SEND_TIMEOUT = 30000;
        public const Int32 MAX_TRIES_COUNT = 30;
        public const Int32 AVATAR_SIDE = 320;
        public const Int32 ORIENTATION_PROPERTY_ID = 274;
    }


}