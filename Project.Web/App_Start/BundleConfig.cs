﻿using System.Web;
using System.Web.Optimization;

namespace Project.Web
{
    public class BundleConfig
    {
        //Дополнительные сведения об объединении см. по адресу: http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));
            bundles.Add(new ScriptBundle("~/bundles/signalr").Include(
                     "~/Scripts/jquery.signalr-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/frotend").Include(


                "~/Scripts/angular/angular-local-storage.js",
               "~/Scripts/angular/angular-route.min.js",
               "~/Scripts/angular/angular-resource.js",

                  "~/Scripts/angular/angular-messages.js",
                  "~/Scripts/ngAutocomplete.js",

                  "~/Scripts/ng-file-upload.js",
                  "~/Scripts/Chart.js",
               "~/Scripts/angular-chart.js",
               "~/Scripts/angular-leaflet-directive.js",
                "~/Scripts/app/app.js",
               "~/Scripts/app/services/authInterceptorService.js",
               "~/Scripts/app/services/authService.js",
               "~/Scripts/app/services/ordersService.js",
               "~/Scripts/app/services/tokensManagerService.js",

               "~/Scripts/app/factory.js",
               "~/Scripts/app/resource.js",
               "~/Scripts/app/controller.js",
               "~/Scripts/app/controllers/login.js",
               "~/Scripts/app/controllers/sugnupController.js",
               "~/Scripts/app/controllers/patientsController.js",
               "~/Scripts/app/controllers/PatientsAddController.js",
               "~/Scripts/app/controllers/PatientsEditController.js",
                "~/Scripts/app/controllers/doctorsController.js",
               "~/Scripts/app/controllers/doctorAddController.js",
               "~/Scripts/app/controllers/doctorEditController.js",
               "~/Scripts/app/controllers/patientController.js",
               "~/Scripts/app/controllers/doctorPatientController.js"
               ));


            // Используйте версию Modernizr для разработчиков, чтобы учиться работать. Когда вы будете готовы перейти к работе,
            // используйте средство сборки на сайте http://modernizr.com, чтобы выбрать только нужные тесты.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                //"~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                "~/Content/mfb.css",
                "~/Scripts/angular-chart.css",
                      "~/Content/site.css"));
        }
    }
}
