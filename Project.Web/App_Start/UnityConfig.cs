using System;
using System.Data.Entity;
using System.Web;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin.Security;
using Microsoft.Practices.Unity;
using Project.Data;
using Project.Data.Infrastructure;
using Project.Data.IRepository;
using Project.Data.Repository;
using Project.Model.Models;
using Project.Service.IService;
using Project.Service.Service;
using Project.Web.Hubs;

namespace Project.Web.App_Start
{
    /// <summary>
    /// Specifies the Unity configuration for the main container.
    /// </summary>
    public class UnityConfig
    {
        #region Unity Container
        private static Lazy<IUnityContainer> container = new Lazy<IUnityContainer>(() =>
        {
            var container = new UnityContainer();
            RegisterTypes(container);
            return container;
        });

        /// <summary>
        /// Gets the configured Unity container.
        /// </summary>
        public static IUnityContainer GetConfiguredContainer()
        {
            return container.Value;
        }
        #endregion

        /// <summary>Registers the type mappings with the Unity container.</summary>
        /// <param name="container">The unity container to configure.</param>
        /// <remarks>There is no need to register concrete types such as controllers or API controllers (unless you want to 
        /// change the defaults), as Unity allows resolving a concrete type even if it was not previously registered.</remarks>
        public static void RegisterTypes(IUnityContainer container)
        {
            // NOTE: To load from web.config uncomment the line below. Make sure to add a Microsoft.Practices.Unity.Configuration to the using statements.
            // container.LoadConfiguration();
            container.RegisterType<IUserStore<ApplicationUser>, UserStore<ApplicationUser>>();
            container.RegisterType<UserManager<ApplicationUser>>();
            container.RegisterType<DbContext, ApplicationDbContext>();
            container.RegisterType<ApplicationUserManager>();

            container.RegisterType<IMessageRepository, MessageRepository>();
            container.RegisterType<IDoctorRepository, DoctorRepository>();
            container.RegisterType<IPatientRepository, PatientRepository>();
            container.RegisterType<IPatientInfoRepository, PatientInfoRepository>();
            container.RegisterType<IDoctorInfoRepository, DoctorInfoRepository>();
            container.RegisterType<IUserRepository, UserRepository>();
            container.RegisterType<IDiseasesRepository, DiseasesRepository>();
            container.RegisterType<IDoctorPatientRepository, DoctorPatientRepository>();
            container.RegisterType<IGpsMessageRepository, GpsMessageRepository>();
            container.RegisterType<IStandartMessageRepository, StandartMessageRepository>();
            container.RegisterType<IInstructionRepository, InstructionRepository>();
            container.RegisterType<IUnitOfWork, UnitOfWork>(new PerRequestLifetimeManager());
            container.RegisterType<IDatabaseFactory, DatabaseFactory>(new PerRequestLifetimeManager());

            container.RegisterType<IMessageService, MessageService>();
           
            container.RegisterType<IPatientService, PatientService>();
            container.RegisterType<IInstructionService, InstructionService>();
            container.RegisterType<IDoctorService, DoctorService>();
            container.RegisterType<IDoctorPatientService, DoctorPatientService>();
            container.RegisterType<SampleHub, SampleHub>(new TransientLifetimeManager());
            container.RegisterType<IAuthenticationManager>(new InjectionFactory(o => HttpContext.Current.GetOwinContext().Authentication));
            // TODO: Register your types here
            // container.RegisterType<IProductRepository, ProductRepository>();
        }
    }
}
