﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Project.Web
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
           routes.MapMvcAttributeRoutes();
            routes.MapRoute(
                name: "template",
                url: "template/{template}",
                defaults: new { controller = "Template", action = "Index"}
            );

            routes.MapMvcAttributeRoutes();
            routes.MapRoute(
                name: "te2mplate",
                url: "image/",
                defaults: new { controller = "Image", action = "post" }
            );
     routes.MapRoute(
   name: "Api1",
   url: "init",
   defaults: new { controller = "Home", action = "Init" ,id=UrlParameter.Optional}
);

            
          
           routes.MapRoute(
        name: "Default1",
        url: "{*.}",
        defaults: new { controller = "Home", action = "Index" }
    );
            
            
        }
    }
}
