﻿var serviceBase = 'http://localhost/';




var app = angular.module('kardiomed', ['ngFileUpload',"leaflet-directive","chart.js",'ngMaterial', 'ngRoute', 'LocalStorageModule', 'UserServices','ngMessages','ng-mfb']);

app.constant('ngAuthSettings', {
    apiServiceBaseUri: serviceBase,
    clientId: 'ngAuthApp'
});



app.config(['$httpProvider',function ($httpProvider) {

    $httpProvider.interceptors.push('authInterceptorService');

}]);

app.run(['authService', function (authService) {
    authService.fillAuthData();
}]);

app.config(['$routeProvider', '$locationProvider','$mdIconProvider','$mdThemingProvider','ChartJsProvider',
    function ($routeProvider, $locationProvider,$mdIconProvider,$mdThemingProvider,ChartJsProvider) {
        ChartJsProvider.setOptions({
            colours: ['#FF5252', '#FF8A80'],
            datasetFill: false,
            responsive: true,
            scaleShowLabels: true,

        });



        $locationProvider.html5Mode(true);

        $mdIconProvider
            .icon("menu"       , "./content/menu.svg", 24)
            .icon("plus"       , "./content/plus-round.svg", 24)
            .icon("close"       , "./content/close.svg", 24);


        $mdThemingProvider.theme('pink')
            .primaryPalette('pink')
            .accentPalette('purple');


        $routeProvider.
            when('/home', {
                templateUrl: 'template/home.html',
                controller: 'HomeController',
                auth: true
            }).
            when('/patients', {
                templateUrl: 'template/patients.html',
                controller: 'patientsController',
                auth: true
            }).
            when('/patient', {
                templateUrl: 'template/patient.html',
                controller: 'patientController',
                auth: true
            }).
            when('/patients/edit', {
                templateUrl: 'template/patient_edit.html',
                controller: 'patientsEditController',
                auth: true
            }).
            when('/patients/add', {
                templateUrl: 'template/patient_edit.html',
                controller: 'patientsAddController',
                auth: true
            }).
            when('/doctors', {
                templateUrl: 'template/doctors.html',
                controller: 'doctorsController',
                auth: true
            }).
            when('/doctor/add', {
                templateUrl: 'template/doctor_edit.html',
                controller: 'doctorAddController',
                auth: true
            }).
            when('/doctor/edit', {
                templateUrl: 'template/doctor_edit.html',
                controller: 'doctorEditController',
                auth: true
            }).
        when('/doctor_patient', {
            templateUrl: 'template/doctor_patient.html',
            controller: 'doctorPatientController',
            auth: true
        })
            .when('/login', {
                templateUrl: 'template/login.html',
                controller: 'loginController'
            }).
            otherwise({
                redirectTo: '/patients'
            });
    }]);


app.run(['$rootScope', '$location', 'authService', "$route", function ($rootScope, $location, authService, $route) {
    $rootScope.$on('$locationChangeStart', function (ev, next, current) {
        var nextPath = $location.path(),
            nextRoute = $route.routes[nextPath];
        if (nextRoute && nextRoute.auth && !authService.authentication.isAuth) {
            $location.path("/login");
        }

    });
}]);




Array.prototype.where = Array.prototype.where || function (predicate) {
        var results = [],
            len = this.length,
            i = 0;

        for (; i < len; i++) {
            var item = this[i];
            if (predicate(item)) {
                results.push(item);
            }
        }

        return results;

    };

