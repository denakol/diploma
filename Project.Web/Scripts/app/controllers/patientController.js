/**
 * Created by Denakol on 30.05.2015.
 */
'use strict';
'use strict';
app.controller('patientController', ['$scope', '$location', 'authService', 'ngAuthSettings', '$mdToast', 'Patient', '$routeParams', 'Doctor', '$mdDialog', function ($scope, $location, authService, ngAuthSettings, $mdToast, Patient, $routeParams, Doctor, $mdDialog) {


    $scope.createTask = function (ev) {
        $mdDialog.show({
            controller: 'TaskCreateController',
            templateUrl: 'template/create_task.html',
            targetEvent: ev,
        })
            .then(function (answer) {
                $scope.alert = 'You said the information was "' + answer + '".';
            }, function () {
                $scope.alert = 'You cancelled the dialog.';
            });
    };
    $scope.selectedIndex = 0;
     $scope.$watch('selectedIndex', function (current, old) {
     if (current == 1) {
     setTimeout(function () {
     window.dispatchEvent(new Event('resize'));
     }, 500)
     }
     });
    $scope.createInstruction = function (ev) {
        $mdDialog.show({
            controller: 'InstructionCreateController',
            templateUrl: 'template/create_instruction.html',
            targetEvent: ev,
        })
            .then(function (answer) {
                $scope.$broadcast('createInstruction', answer);
            }, function () {
                $scope.alert = 'You cancelled the dialog.';
            });
    };


    $scope.patient = Patient.getSummary({Id: $routeParams.id});


    $scope.patient.$promise.then(function () {
        if ($scope.patient.disease == undefined) {
            $scope.patient.disease = [];
        }
        $scope.patient.birthday = new Date($scope.patient.birthday);
        $scope.patient.registerDate = new Date($scope.patient.registerDate);
    });
    $scope.mode = 'edit';

    $scope.doctors = Doctor.query();
    $scope.legend = "3"

    $scope.save = function () {
        Patient.update($scope.patient).$promise.then(function (res) {
            $scope.patient.birthday = new Date($scope.patient.birthday);
            $scope.patient.registerDate = new Date($scope.patient.registerDate);
            $location.path('/patients');
        });
        ;
    };

    $scope.remove = function () {
        $scope.patient.$remove({id: $scope.patient.id}).then(function (res) {
            $location.path('/patients');
        });
        ;
    };


    $scope.series = ['Series A'];


    setTimeout(function () {
        window.dispatchEvent(new Event('resize'));
    }, 500)


}]);
app.controller('TaskCreateController', ['$scope', '$location', 'authService', 'ngAuthSettings', '$mdToast', 'Patient', '$routeParams', 'Doctor', '$mdDialog', function ($scope, $location, authService, ngAuthSettings, $mdToast, Patient, $routeParams, Doctor, $mdDialog) {

    $scope.hide = function () {
        $mdDialog.hide();
    };
    $scope.cancel = function () {
        $mdDialog.cancel();
    };
    $scope.answer = function (answer) {
        $mdDialog.hide(answer);
    };

}]);



app.controller('patientMonitorController', ['$scope', '$location', 'authService', 'ngAuthSettings', '$mdToast', 'Patient', '$routeParams', 'Doctor', 'GpsMessage', 'StandartMessage', '$filter', function ($scope, $location, authService, ngAuthSettings, $mdToast, Patient, $routeParams, Doctor, GpsMessage, StandartMessage, $filter) {

    var self = this;
    self.gpsMessages = GpsMessage.getSummary({Id: $routeParams.id});

    angular.extend($scope, {
        center: {},
        paths: {},
        markers:{}
    });


    self.gpsMessages.$promise.then(function () {
        if (self.gpsMessages.length > 0) {
            $scope.center={
                lat: self.gpsMessages[0].latitude,
                lng: self.gpsMessages[0].longitude,
                zoom: 14
            };




            $scope.paths.polyline={};
            $scope.paths.polyline.type="polyline";
            $scope.paths.polyline.latlngs=[];
            angular.forEach( self.gpsMessages,function (item,index){
                $scope.paths.polyline.latlngs.push({
                    lat: item.latitude,
                    lng: item.longitude
                    })

                if(index==0)
                {
                    $scope.markers.main_marker= {
                        lat: item.latitude,
                        lng: item.longitude,
                        focus: true,
                        message: "Последняя позиция",
                        title: "Последняя позиция",
                        draggable: false,
                        label: {
                            message: "Последняя позиция",
                            options: {
                                noHide: true
                            }
                        }
                    }
                }
                else if(index ==self.gpsMessages.length-1)
                {
                    $scope.markers.start = {
                        lat: item.latitude,
                        lng: item.longitude,
                        message: "Начальная позиция",
                        title: "Начальная позиция",
                        draggable: false,
                        label: {
                            message: "Начальная позиция",
                            options: {
                                noHide: true
                            }
                        }
                    }
                } else {
                    $scope.markers[index+"mark"] = {
                        lat: item.latitude,
                        lng: item.longitude,

                    }
                }



            });


        }


    });
    $scope.option_ecg = {
        pointDot: false,
        scaleShowHorizontalLines: false,
        scaleShowVerticalLines: false,
        showTooltips: false,
        responsive: true,
        scaleShowLabels: false
    }
    $scope.option_heartRate = {
        pointDot: false,
        scaleShowHorizontalLines: true,
        scaleShowVerticalLines: true,
        responsive: true,
        scaleShowGridLines : true,
        scaleSteps : 5,
        scaleStepWidth : 10,
        scaleStartValue : 40,
        scaleOverride   : true,
        bezierCurve:false

    }


    self.standartMessages = StandartMessage.getSummary({Id: $routeParams.id});
    self.standartMessages.$promise.then(function () {
        $scope.data_ecg = [self.standartMessages.lastExtend];
        if ($scope.data_ecg.length > 0) {
            var date = new Date(self.standartMessages.dateTime)
            $scope.dateLastEcg = $filter('date')(date, "dd:mm HH:mm:ss");
        }
        $scope.labels_ecg = [];
        var count = 0;


        while ($scope.labels_ecg.length <= $scope.data_ecg[0].length) {
            var k = count % 4;
            if (count % 16 == 0) {
                $scope.labels_ecg.push(count);

            }
            $scope.labels_ecg.push("");
            count++;
        }


        $scope.data_heartRate = [self.standartMessages.heartRate];

        $scope.labels_heartRate = [];
        count = $scope.data_heartRate[0].length - 1;
        while (count >= 0) {

            var date = new Date(self.standartMessages.dateTimes[count])
            $scope.labels_heartRate.push($filter('date')(date, "HH:mm:ss"));
            count--;
        }

    })


}]);
app.controller('patientInfoController', ['$scope', '$location', 'authService', 'ngAuthSettings', '$mdToast', 'Patient', '$routeParams', 'Doctor', function ($scope, $location, authService, ngAuthSettings, $mdToast, Patient, $routeParams, Doctor) {


}]);

app.controller('patientTaskController', ['$scope', '$location', 'authService', 'ngAuthSettings', '$mdToast', 'Patient', '$routeParams', 'Doctor', function ($scope, $location, authService, ngAuthSettings, $mdToast, Patient, $routeParams, Doctor) {

    $scope.tasks = []

    while ($scope.tasks.length < 10) {
        $scope.tasks.push({
            title: 'Название задания',
            type: 'Тип задания',
            description: "Описание задания",
            Id: "ID",
            comment: "комментарий пациента"
        })
    }

}]);

app.controller('patientInstructionController', ['$scope', '$location', 'authService', 'ngAuthSettings', '$mdToast', 'Patient', '$routeParams', 'Doctor','Instruction', function ($scope, $location, authService, ngAuthSettings, $mdToast, Patient, $routeParams, Doctor,Instruction) {

    $scope.instructions =Instruction.query({Id: $routeParams.id});
    $scope.$on('createInstruction', function (evnt, data) {
        $scope.instructions.push( data);
    });
 /*   while ($scope.instructions.length < 10) {
        $scope.instructions.push({
            title: 'Название предписани',
            description: "Описание предписания",
            Id: "ID",
            comment: "комментарий пациента"
        })
    }*/

}]);

app.controller('InstructionCreateController', ['$scope', '$location', 'authService', 'ngAuthSettings', '$mdToast', 'Patient', '$routeParams', 'Doctor', '$mdDialog','Instruction', function ($scope, $location, authService, ngAuthSettings, $mdToast, Patient, $routeParams, Doctor, $mdDialog,Instruction) {




    $scope.instruction = new Instruction();
    $scope.hide = function () {
        $mdDialog.hide();
    };
    $scope.cancel = function () {
        $mdDialog.cancel();
    };
    $scope.answer = function () {
        $scope.instruction.patientId= $routeParams.id;
        $scope.instruction.$save().then(function(res){
            $mdDialog.hide($scope.instruction);
        });
    };

}]);

