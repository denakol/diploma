/**
 * Created by Denakol on 29.05.2015.
 */
'use strict';
app.controller('doctorsController', ['$scope', '$location', 'authService', 'ngAuthSettings', '$mdToast','Patient','$routeParams','Doctor', function ($scope, $location, authService, ngAuthSettings, $mdToast,Patient,$routeParams,Doctor) {

    $scope.doctors = Doctor.query();

    $scope.add=function(){
      $location.path("/doctor/add")
    };

    $scope.remove=function(doctor){
        doctor.$remove({id:doctor.id});
    };
}]);