/**
 * Created by Denakol on 29.05.2015.
 */
/**
 * Created by Denakol on 29.05.2015.
 */
'use strict';
app.controller('doctorAddController', ['$scope', '$location', 'authService', 'ngAuthSettings', '$mdToast','Patient','$routeParams','Doctor','Upload', function ($scope, $location, authService, ngAuthSettings, $mdToast,Patient,$routeParams,Doctor,Upload) {

    $scope.doctor = new Doctor();
    $scope.mode='add';
    $scope.add = function()
    {
        $scope.doctor.$save().then(function(res){
            $scope.doctor.birthday = new Date($scope.doctor.birthday);
            $scope.doctor.registerDate = new Date($scope.doctor.registerDate);
            $location.path('/doctors');
        });
    };


    $scope.cancel = function()
    {
        $location.path('/doctors');
    };

    $scope.fileSelected = function ($files, user) {
        //$files: an array of files selected, each file has name, size, and type.
        for (var i = 0; i < $files.length; i++) {
            var file = $files[i];
            var upload = Upload.upload({
                url: 'Image',
                data: { name: user.Name },
                file: file, // or list of files ($files) for html5 only
            }).progress(function (evt) {
                //console.log('percent: ' + parseInt(100.0 * evt.loaded / evt.total));
            }).success(function (data, status, headers, config) {
                $scope.doctor.href = data.imagePath;
            }).error(function (err) {
                alert('Error occured during upload');
            });
        }
    };
}]);