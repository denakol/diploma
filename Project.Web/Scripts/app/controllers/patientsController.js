/**
 * Created by denak on 27.05.2015.
 */
'use strict';
app.controller('patientsController', ['$scope', '$location', 'authService', 'ngAuthSettings', '$mdToast','Patient','$routeParams','Doctor', function ($scope, $location, authService, ngAuthSettings, $mdToast,Patient,$routeParams,Doctor) {
    $scope.patients = Patient.query();

    $scope.role = authService.authentication.role;
    $scope.add=function() {
        $location.path("/patients/add");
    };

    $scope.remove=function(patient){
        patient.$remove({id:patient.id});
    };
}]);
