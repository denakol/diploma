/**
 * Created by Denakol on 01.06.2015.
 */
app.controller('doctorPatientController', ['$scope', '$location', 'authService', 'ngAuthSettings', '$mdToast','Patient','$routeParams','Doctor','DoctorPatient','$q', function ($scope, $location, authService, ngAuthSettings, $mdToast,Patient,$routeParams,Doctor,DoctorPatient,$q) {






    $scope.doctors = DoctorPatient.query();



    $scope.edit= function(doctor)
    {
        doctor.tempPatients = angular.copy(doctor.patients);
        doctor.isEdit=true;
    }

    $scope.save= function(doctor)
    {
        DoctorPatient.update(doctor).$promise.then(function()
        {
            doctor.isEdit=false;
        });

    }

    $scope.cancel = function(doctor)
    {
        doctor.patients = angular.copy(doctor.tempPatients);
        doctor.isEdit=false;
    }



    var self = this;
    self.allPatients=Patient.query();
    self.allPatients.$promise.then(function()
    {
        angular.forEach(self.allPatients,function(item)
        {
            item._lowername=item.fullName.toLowerCase();
        })
    });

    $scope.filterSelected = true;

    $scope.remove=function(doctor){
        doctor.$remove({id:doctor.id});
    };

   $scope.querySearch=function  (query) {
        var results = query ?
            self.allPatients.filter(createFilterFor(query)) : [];
        return results;
    }
    /**
     * Create filter function for a query string
     */
    function createFilterFor(query) {
        var lowercaseQuery = angular.lowercase(query);
        return function filterFn(contact) {
            return (contact._lowername.indexOf(lowercaseQuery) != -1);;
        };
    }

    $scope.readonly=true;
    $scope.patients=[];
    for(var k=0;k<10;k++)
    {
        $scope.patients.push({
            fullName:"Акользин Денис"+k,
            href:"images/sample.png",
            city:"Taganrog"+k,
            _lowername:("(Акользин Денис"+k).toLowerCase()
        })
    }
}]);