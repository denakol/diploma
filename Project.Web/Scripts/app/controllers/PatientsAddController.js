/**
 * Created by Denakol on 29.05.2015.
 */
'use strict';
app.controller('patientsAddController', ['$scope', '$location', 'authService', 'ngAuthSettings', '$mdToast','Patient','$routeParams','Doctor', 'Upload', function ($scope, $location, authService, ngAuthSettings, $mdToast,Patient,$routeParams,Doctor,Upload) {


    $scope.doctors = Doctor.query();
    $scope.patient = new Patient();
    $scope.patient.disease=[];
    $scope.mode='add';

    $scope.add = function()
    {
        $scope.patient.$save().then(function(res){
            $scope.patient.birthday = new Date($scope.patient.birthday);
            $scope.patient.registerDate = new Date($scope.patient.registerDate);
            $location.path('/patients');
        });
    };

    $scope.cancel = function()
    {
       $location.path('/patients');
    };

    $scope.fileSelected = function ($files, user) {
        //$files: an array of files selected, each file has name, size, and type.
        for (var i = 0; i < $files.length; i++) {
            var file = $files[i];
            var upload = Upload.upload({
                url: 'Image',
                data: { name: user.Name },
                file: file, // or list of files ($files) for html5 only
            }).progress(function (evt) {
                //console.log('percent: ' + parseInt(100.0 * evt.loaded / evt.total));
            }).success(function (data, status, headers, config) {
                $scope.patient.href = data.imagePath;
            }).error(function (err) {
                alert('Error occured during upload');
            });
        }
    };
}]);