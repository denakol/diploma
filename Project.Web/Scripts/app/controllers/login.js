﻿'use strict';
app.controller('loginController', ['$scope', '$location', 'authService', 'ngAuthSettings', '$mdToast', function ($scope, $location, authService, ngAuthSettings, $mdToast) {

    $scope.loginData = {
        userName: "",
        password: "",
        useRefreshTokens: false
    };

    $scope.toastPosition = {
        bottom: false,
        top: true,
        left: false,
        right: true
    };
    $scope.getToastPosition = function () {
        return Object.keys($scope.toastPosition)
          .filter(function (pos) { return $scope.toastPosition[pos]; })
          .join(' ');
    };

    $scope.message = "";

    $scope.login = function () {
        authService.login($scope.loginData).then(function (response) {
            $location.path('/patients');
        },
         function (err) {
             showSimpleToast(err);
         });
    };

    function showSimpleToast (err) {
        $mdToast.show(
          $mdToast.simple()
            .content(err.error_description)
            .position($scope.getToastPosition())
            .hideDelay(3000)
        );
    };

    $scope.authCompletedCB = function (fragment) {

        $scope.$apply(function () {

            if (fragment.haslocalaccount == 'False') {

                authService.logOut();

                authService.externalAuthData = {
                    provider: fragment.provider,
                    userName: fragment.external_user_name,
                    externalAccessToken: fragment.external_access_token
                };

                $location.path('/associate');

            }
            else {
                //Obtain access token and redirect to orders
                var externalData = { provider: fragment.provider, externalAccessToken: fragment.external_access_token };
                authService.obtainAccessToken(externalData).then(function (response) {

                    $location.path('/patients');

                },
             function (err) {
                 $scope.message = err.error_description;
             });
            }

        });
    }
}]);
