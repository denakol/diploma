﻿angular.module('UserServices', ['ngResource'])
    .factory('Patient', ['$resource', function ($resource) {
        return $resource('/api/patient/:Id/:Action', {}, {
            update: {method: 'PUT', params: {Id: '@id'}},
            getSummary: {method: 'Get', params: {Action: 'summary'}}
        });
    }])
    .factory('Doctor', ['$resource', function ($resource) {
        return $resource('/api/doctor/:Id', {}, {
            update: {method: 'PUT', params: {Id: '@id'}},
        });
    }]).factory('DoctorPatient', ['$resource', function ($resource) {
        return $resource('/api/doctorPatient/:Id', {}, {
            update: {method: 'PUT', params: {Id: '@id'}},
        });
    }]).factory('GpsMessage', ['$resource', function ($resource) {
        return $resource('/api/GpsMessage/:Id', {}, {
            getSummary: {method: 'GET', isArray: true}
        });
    }]).factory('StandartMessage', ['$resource', function ($resource) {
        return $resource('/api/StandartMessage/:Id', {}, {
            getSummary: {method: 'GET', isArray: false}
        });
    }]).factory('Instruction', ['$resource', function ($resource) {
        return $resource('/api/Instruction/:Id',{}, {

        });
    }]);
