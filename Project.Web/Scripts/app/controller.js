﻿﻿'use strict';
var myMap;
app.controller('AppController', ['$scope', '$mdSidenav','backendHubProxy','authService','$location',
  function ($scope, $mdSidenav, backendHubProxy, authService,$location) {
      $scope.toggleSidenav = toggleSidenav;

      $scope.authentication = authService.authentication;

      function toggleSidenav(name) {
          $mdSidenav(name).toggle();
      }

      $scope.logout = function() {
          authService.logOut();
          $location.path('/login');
      };
      $scope.isActive = function (viewLocation) {
          var active = (viewLocation === $location.path());
          return active;
      };

      var webSocket;
      var handlerUrl = "ws://localhost:64270/Flussonic.ashx";
      InitWebSocket();

      function CloseWebSocket() {
          //Функция для программного закрытия веб-сокета.
          //После закрытия, получать или отправлять данные не получится.
          webSocket.close();
      }
      function InitWebSocket() {
          //Если объект веб-сокета не инициализирован, инициализируем его.
          if (webSocket == undefined) {
              webSocket = new WebSocket(handlerUrl);

              //Устанавливаем обработчик открытия соединения.
              webSocket.onopen = function () {
                  webSocket.send(JSON.stringify({
                      login:"arlek",
                      password:"fgjfgfkjrrifgsde",
                      host: "95.211.220.38:8080"
                  }));
              };

              //Устанавливаем обработчик получения данных.
              webSocket.onmessage = function (e) {

              };

              //Устанавливаем обработчик закрытия соединения.
              webSocket.onclose = function () {

              };

              //Устанавливаем обработчик ошибки.
              webSocket.onerror = function (e) {

              }
          }
      }


  }
]);

app.controller('HomeController', ['$scope', '$mdSidenav', 'backendHubProxy',
  function ($scope, $mdSidenav, backendHubProxy) {
     
  }
]);


