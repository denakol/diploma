﻿'use strict';

app.factory('backendHubProxy', ['$rootScope', 
  function ($rootScope) {
      function backendFactory(serverUrl, hubName) {
          var connection = $.hubConnection();
          var proxy = connection.createHubProxy(hubName);
          var startPromise = proxy.connection.start();
          return {
              on: function (eventName, callback) {
                  proxy.on(eventName, function (result) {
                      $rootScope.$apply(function () {
                          if (callback) {
                              callback(result);
                          }
                      });
                  });
              },
              invoke: function (methodName, callback) {

                  startPromise.done(function() {
                      proxy.invoke(methodName)
                          .done(function(result) {
                              $rootScope.$apply(function() {
                                  if (callback) {
                                      callback(result);
                                  }
                              });
                          });
                  });


              },
              invokeWithParam: function (methodName,param, callback) {
                  proxy.invoke(methodName, param)
              .done(function (result) {
                  $rootScope.$apply(function () {
                      if (callback) {
                          callback(result);
                      }
                  });
              });
          }
          };
      };
      return backendFactory;
  }]);