﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Project.Service.DTO;
using Project.Service.IService;

namespace Project.Service.Service
{
    public class FakeDoctorService : IDoctorService
    {
        public List<SummaryOfDoctor> GetAllDoctors()
        {
            List<SummaryOfDoctor> doctor = new List<SummaryOfDoctor>();

            while (doctor.Count < 10)
            {
                doctor.Add(new SummaryOfDoctor()
                {
                    About = "Информация о болезнях и все все такое",
                    Address = "г.Таганрог",
                    Age = 32,
                   
                    FullName = "Акользин Денис",
                    Href = "sample.png",
                    Education = "высшее та то там",
                    Patients = new List<UserInfo>()
                    {
                        new UserInfo() { FullName = "Акользин Антон", Href = "sample.png",  },
                        new UserInfo() { FullName = "Акользин Антон", Href = "sample.png",  }, 
                        new UserInfo() { FullName = "Акользин Антон", Href = "sample.png",  },
                        new UserInfo() { FullName = "Акользин Антон", Href = "sample.png", },
                        new UserInfo() { FullName = "Акользин Антон", Href = "sample.png",  }
                    }

                });
            }
            return doctor;
        }

        AddDoctor IDoctorService.GetDoctor(string idDoctor)
        {
            throw new NotImplementedException();
        }

        public SummaryOfDoctor GetDoctor(string idDoctor)
        {
            throw new NotImplementedException();
        }

        public SummaryOfDoctor GetDoctor(int idDoctor)
        {
            return null;
        }

        public bool CreateDoctor(AddDoctor doctor)
        {
            return false;
        }

        public SummaryOfDoctor GetSummaryDoctor(string idDoctor)
        {
            throw new NotImplementedException();
        }

        public void UpdateDoctor(AddDoctor doctor)
        {
            throw new NotImplementedException();
        }

        public void RemoveDoctor(string id)
        {
            throw new NotImplementedException();
        }
    }
}
