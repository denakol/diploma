﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Project.Service.DTO;
using Project.Service.IService;

namespace Project.Service.Service
{
    public class FakePatientService :IPatientService
    {
        public List<SummaryOfPatient> GetAllPatient(int idDoctor)
        {

            List<SummaryOfPatient> patients = new List<SummaryOfPatient>();

            while (patients.Count<10)
            {
                patients.Add(new SummaryOfPatient()
                {
                    About = "Информация о болезнях и все все такое",
                    Address = "г.Таганрог",
                    Age = 32,
                    DateLastData = DateTime.Now,
                    FullName = "Акользин Денис",
                    Href = "sample.png",
                  
                });
            }
            return patients;
        }

        public SummaryOfPatient GetPatient(int idpatient)
        {
            return null;
        }

        public List<SummaryOfPatient> GetAllPatients()
        {
            throw new NotImplementedException();
        }

        public List<SummaryOfPatient> GetPatientByDoctor(string idDoctor)
        {
            throw new NotImplementedException();
        }

        AddPatient IPatientService.GetPatient(string idpatient)
        {
            throw new NotImplementedException();
        }

        public SummaryOfPatient GetSummaryPatient(string idpatient)
        {
            throw new NotImplementedException();
        }

        public SummaryOfPatient GetPatient(string idpatient)
        {
            throw new NotImplementedException();
        }

        Task<bool> IPatientService.CreatePatient(AddPatient patient)
        {
            throw new NotImplementedException();
        }

        public void UpdatePatient(AddPatient patient)
        {
            throw new NotImplementedException();
        }

        public void RemovePatient(string id)
        {
            throw new NotImplementedException();
        }

        public Task CreatePatient(AddPatient patient)
        {

            return Task.Run(() => { });
        }
    }
}
