﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Project.Data.Infrastructure;
using Project.Data.IRepository;
using Project.Model.Models;
using Project.Service.DTO;
using Project.Service.IService;

namespace Project.Service.Service
{
    public class InstructionService : IInstructionService
    {
        private readonly IDoctorRepository _doctorRepository;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IPatientRepository _patientRepository;
        private readonly IInstructionRepository _instructionRepository;
        public InstructionService(IInstructionRepository instructionRepository, IPatientRepository patientRepository, IDoctorRepository focusRepository, IUnitOfWork unitOfWork)
        {
            _doctorRepository = focusRepository;
            _unitOfWork = unitOfWork;
            _patientRepository = patientRepository;
            _instructionRepository = instructionRepository;
        }


        public IEnumerable<InstructionModel> GetByUser(String userId)
        {
            return _instructionRepository.GetMany(x => x.PatientId == userId).Select(x => new InstructionModel()
            {
                Description = x.Description,
                DoctorId = x.DoctorId,
                EndDate = x.EndDate,
                StartDate = x.StartDate,
                Title = x.Title,
                PatientId = x.PatientId
            }).ToList();
        }

        public void Archive(int id)
        {
            _instructionRepository.Delete(x => x.Id == id);
            _unitOfWork.Commit();
        }

        public void CreateInstruction(InstructionModel instructionModel)
        {
            var doctor = _doctorRepository.GetById(instructionModel.DoctorId);


            if (doctor == null)
            {
                doctor = _doctorRepository.GetAll().FirstOrDefault();
            }

            _instructionRepository.Add(new Instruction()
            {
                Description = instructionModel.Description,
                DoctorId = doctor.Id,
                EndDate = instructionModel.EndDate,
                StartDate = instructionModel.StartDate,
                Title = instructionModel.Title,
                PatientId = instructionModel.PatientId
            });
           
            _unitOfWork.Commit();
        }
    }
}
