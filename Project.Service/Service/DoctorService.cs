﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Project.Data.Infrastructure;
using Project.Data.IRepository;
using Project.Model.Models;
using Project.Service.DTO;
using Project.Service.IService;

namespace Project.Service.Service
{
    public class DoctorService : IDoctorService
    {


        private readonly IDoctorRepository _doctorRepository;
        private readonly IUnitOfWork _unitOfWork;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IPatientRepository _patientRepository;
        private readonly IDoctorInfoRepository _doctorInfoRepository;
        private readonly IUserRepository _userRepository;
        public DoctorService(IPatientRepository patientRepository,IDoctorInfoRepository doctorInfoRepository,IUserRepository userRepository, IDoctorRepository focusRepository, IUnitOfWork unitOfWork, UserManager<ApplicationUser> userManager)
        {
            _doctorRepository = focusRepository;
            _unitOfWork = unitOfWork;
            _userManager = userManager;
            _userRepository = userRepository;
            _doctorInfoRepository=doctorInfoRepository;
            _patientRepository = patientRepository;
        }
        public List<SummaryOfDoctor> GetAllDoctors()
        {
            var doctors= _doctorRepository.GetAll().Select(x=>new SummaryOfDoctor()
            {
                About = x.About,
                Address = x.Address,
                Age = DateTime.Now.Year-x.Brithday.Year,
                FullName = x.LastName + " " + x.FirstName + " " + x.SemiName,
                Id=x.Id,
                Href = x.Href
            }).ToList();
            foreach (var summaryOfDoctor in doctors)
            {
                var doctor = summaryOfDoctor;
                var doctorInfo = _doctorInfoRepository.Get(x => x.Id == doctor.Id);
                summaryOfDoctor.Education = doctorInfo.Education;
                summaryOfDoctor.Patients =
                    _patientRepository.GetByIdDoctor(summaryOfDoctor.Id).Select(x => new UserInfo() {FullName = x.LastName + " " + x.FirstName + " " + x.SemiName,Id = x.Id})
                        .ToList();
            }
            return doctors;
        }

        public SummaryOfDoctor GetSummaryDoctor(String idDoctor)
        {
            return GetAllDoctors().FirstOrDefault(x => x.Id == idDoctor);
        }

        public void UpdateDoctor(AddDoctor doctor)
        {
            var doctorEntity = _doctorRepository.GetById(doctor.Id);
            doctorEntity.About = doctor.About;

            doctorEntity.Address = doctor.Address;
            doctorEntity.Brithday = doctor.Birthday;
            doctorEntity.DateRegister = doctor.RegisterDate;
            doctorEntity.FirstName = doctor.FirstName;
            doctorEntity.LastName = doctor.LastName;
            doctorEntity.SemiName = doctor.SemiName;
            doctorEntity.IsMale = doctor.isMale;
            doctorEntity.UserName = doctor.UserName;
            doctorEntity.Href = doctor.Href;
            _doctorRepository.Update(doctorEntity);



            var doctorinfo = _doctorInfoRepository.GetById(doctorEntity.Id);
            doctorinfo.Education = doctor.Education;
            _doctorInfoRepository.Update(doctorinfo);
            _unitOfWork.Commit();
            if (!String.IsNullOrEmpty(doctor.Password))
            {
                string resetToken = _userManager.GeneratePasswordResetToken(doctor.Id);
                _userManager.ResetPassword(doctor.Id, resetToken, doctor.Password);

            }


        }

        public void RemoveDoctor(string id)
        {

            var user = _userManager.FindById(id);


            _doctorInfoRepository.Delete(x=>x.Id==id);
            _unitOfWork.Commit();

            var rolesForUser = _userManager.GetRoles(id);
            if (rolesForUser.Any())
            {
                foreach (var item in rolesForUser.ToList())
                {
                    // item should be the name of the role
                    _userManager.RemoveFromRole(id, item);
                }
            }
            _userManager.Delete(user);
        }


        public AddDoctor GetDoctor(String idDoctor)
        {
            var doctor = _doctorRepository.GetById(idDoctor);
            var education = _doctorInfoRepository.Get(x => x.Id == idDoctor).Education;
            return new AddDoctor()
            {
                About = doctor.About,
                Address = doctor.Address,
                Birthday = doctor.Brithday,
                Education = education,
                FirstName = doctor.FirstName,
                LastName = doctor.LastName,
                Password = "",
                RegisterDate = doctor.DateRegister,
                SemiName = doctor.SemiName,
                UserName = doctor.UserName,
                Id = doctor.Id,
                Href = doctor.Href

            };
        }

        public  Boolean CreateDoctor(AddDoctor doctor)
        {
            var appUser = new ApplicationUser()
            {
                UserName = doctor.UserName,
                About = doctor.About,
                Brithday = doctor.Birthday,
                FirstName = doctor.FirstName,
                LastName = doctor.LastName,
                SemiName = doctor.SemiName,
                DateRegister = doctor.RegisterDate,
                Href = doctor.Href

            };
            var result = _userManager.Create(appUser, doctor.Password);
            if (result.Succeeded)
            {
                var user = _userRepository.Get(x=>x.UserName ==doctor.UserName);
                var addRoleResult = _userManager.AddToRoles(user.Id, new[] { "Doctor" });
                if (addRoleResult.Succeeded)
                {
                    _doctorInfoRepository.Add(new DoctorInfo(){Doctor = user,Education = doctor.Education});
                  
                }
                _unitOfWork.Commit();

            }
            return true;
        }
    }
}
