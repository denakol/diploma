﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using Project.Data.Infrastructure;
using Project.Data.IRepository;
using Project.Model.Models;
using Project.Service.DTO;
using Project.Service.IService;

namespace Project.Service.Service
{
    public class MessageService : IMessageService
    {

        private Int32 CountGps = 20;
        private Int32 HeartRateCount = 24;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IStandartMessageRepository _standartMessageRepository;
        private readonly IGpsMessageRepository _gpsMessageRepository;
        private readonly IPatientRepository _patientRepository;
        public MessageService(IPatientRepository patientRepository,IStandartMessageRepository standartMessageRepository, IGpsMessageRepository gpsMessageRepository, IUnitOfWork unitOfWork)
        {
            _standartMessageRepository = standartMessageRepository;
            _gpsMessageRepository = gpsMessageRepository;
            _unitOfWork = unitOfWork;
            _patientRepository=patientRepository ;
        }

        public void LogMessage(string message)
        {
            _unitOfWork.Commit();
        }

        public List<Message> GetAll()
        {
            return null;
        }

        public void CreateStandartMessage(string id, StandartMessageDTO value)
        {
            if (_patientRepository.GetById(id) == null)
            {
                return;
            }
            var standart = new StandartMessage
            {
                Data = value.Extend.Select(x => (double)x).ToArray(),
                HeartRate = value.HeartRate,
                DateTime = DateTime.Now.ToUniversalTime(),
                PatientId = id
            };
            _standartMessageRepository.Add(standart);
            _unitOfWork.Commit();
        }

        public void CreateGpsMessage(string id, GpsMessageDTO value)
        {
            if (_patientRepository.GetById(id) == null)
            {
                return;
            }
            var gpsMessage = new GpsMessage()
            {
                Latitude = value.Latitude,
                Longitude = value.Longitude,
                PatientId = id,
                Speed = value.Speed,
                DateTime = DateTime.Now.ToUniversalTime()
            };
            _gpsMessageRepository.Add(gpsMessage);
            _unitOfWork.Commit();
        }

        public IEnumerable<GpsMessageDTO> GetAllGpsMessageForUser(string userId)
        {
            return _gpsMessageRepository.GetMany(x => x.PatientId == userId).Select(x => new GpsMessageDTO()
            {
                Latitude = x.Latitude,
                Longitude = x.Longitude,
                Speed = x.Speed,
                DateTime = x.DateTime
            }).ToList();
        }

        public IEnumerable<StandartMessageDTO> GetAllStandartMessageForUser(string userId)
        {
            return _standartMessageRepository.GetMany(x => x.PatientId == userId).Select(x => new StandartMessageDTO()
            {
                HeartRate = x.HeartRate,
                Extend = x.Data.Select(y => (int)y).ToArray(),

            }).ToList();
        }

        public IEnumerable<GpsMessageDTO> GetGpsMessageForUserSummaryPage(string userId)
        {

            return _gpsMessageRepository.GetMany(x => x.PatientId == userId).OrderByDescending(x => x.DateTime).Take(CountGps).Select(x => new GpsMessageDTO()
            {
                Latitude = x.Latitude,
                Longitude = x.Longitude,
                Speed = x.Speed,
                DateTime = x.DateTime
            }).ToList();
        }

        public SummaryStandartMessage GetStandartMessageForUserSummaryPage(string userId)
        {
            var messages = _standartMessageRepository.GetMany(x => x.PatientId == userId).OrderByDescending(x => x.DateTime).Take(HeartRateCount).ToList();
            var summary = new SummaryStandartMessage()
            {
                HeartRate = messages.Select(x => x.HeartRate),
                DateTimes = messages.Select(x=>x.DateTime),
                LastExtend = messages.FirstOrDefault() != null ? messages.FirstOrDefault().Data.Select(x=>(int)x):null,
                DateTime = messages.FirstOrDefault() != null ? messages.FirstOrDefault().DateTime : DateTime.Now
            };
            return summary;
        }
    }
}
