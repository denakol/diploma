﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Project.Data.Infrastructure;
using Project.Data.IRepository;
using Project.Model.Models;
using Project.Service.DTO;
using Project.Service.IService;

namespace Project.Service.Service
{
    public class DoctorPatientService : IDoctorPatientService
    {


        private readonly IDoctorRepository _doctorRepository;
        private readonly IUnitOfWork _unitOfWork;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IPatientRepository _patientRepository;
        private readonly IDoctorInfoRepository _doctorInfoRepository;
        private readonly IUserRepository _userRepository;
        private readonly IDoctorPatientRepository _doctorPatientRepository;
        public DoctorPatientService(IDoctorPatientRepository repository,IPatientRepository patientRepository, IDoctorInfoRepository doctorInfoRepository, IUserRepository userRepository, IDoctorRepository focusRepository, IUnitOfWork unitOfWork, UserManager<ApplicationUser> userManager)
        {
            _doctorRepository = focusRepository;
            _unitOfWork = unitOfWork;
            _userManager = userManager;
            _userRepository = userRepository;
            _doctorInfoRepository = doctorInfoRepository;
            _patientRepository = patientRepository;
            _doctorPatientRepository = repository;
        }

        public IEnumerable<DoctorPatientInfo> GetAll()
        {
            var doctors = _doctorRepository.GetAll().Select(x=>new DoctorPatientInfo()
            {
                FullName = x.LastName+" "+x.FirstName+" "+x.SemiName,
                Href = x.Href,
                Id=x.Id,
                Patients = new List<PatientItem>()
            }).ToList();

            foreach (var doctorPatientInfo in doctors)
            {
                doctorPatientInfo.Patients =
                    _patientRepository.GetByIdDoctor(doctorPatientInfo.Id).Select(x => new PatientItem()
                    {
                       Address = x.Address,
                       FullName = x.LastName + " " + x.FirstName + " " + x.SemiName,
                       Href = x.Href,
                       Id=x.Id
                    });
            }
            return doctors;
        }

        public void Update(DoctorPatientInfo value)
        {
            var doctor = _doctorRepository.GetById(value.Id);

            foreach (var patient in doctor.Patients.ToList())
            {
                _doctorPatientRepository.Delete(patient);
            }
            _unitOfWork.Commit();
            doctor.Patients = new List<DoctorPatient>(value.Patients.Select(x=>new DoctorPatient()
            {
                PatientId = x.Id
            }));
            _doctorRepository.Update(doctor);
            _unitOfWork.Commit();
        }
    }
}
