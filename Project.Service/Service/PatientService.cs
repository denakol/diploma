﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security.DataProtection;
using Project.Data.Infrastructure;
using Project.Data.IRepository;
using Project.Model.Models;
using Project.Service.DTO;
using Project.Service.IService;

namespace Project.Service.Service
{
    public class PatientService: IPatientService
    {


         private readonly IPatientRepository _patientRepository;
        private readonly IUnitOfWork _unitOfWork;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IPatientInfoRepository _patientInfoRepository;
        private readonly IUserRepository _userRepository;
        private readonly IDoctorRepository _doctorRepository;
        private readonly IDiseasesRepository _diseasesRepository;
        public PatientService(IDiseasesRepository diseasesRepository, IDoctorRepository doctorRepository, IPatientInfoRepository patientInfoRepository, IUserRepository userRepository, IPatientRepository patientRepository, IUnitOfWork unitOfWork, UserManager<ApplicationUser> userManager)
        {
            _patientRepository = patientRepository;
            _unitOfWork = unitOfWork;
            _userManager = userManager;
            _userRepository = userRepository;
            _patientInfoRepository = patientInfoRepository;
            _doctorRepository = doctorRepository;
            _diseasesRepository = diseasesRepository;
        }

    

        public List<SummaryOfPatient> GetPatientByDoctor(String idDoctor)
        {
            var patients = _patientRepository.GetByIdDoctor(idDoctor).Select(x => new SummaryOfPatient()
            {
                About = x.About,
                Address = x.Address,
                Age = DateTime.Now.Year - x.Brithday.Year,
                FullName = x.LastName + " " + x.FirstName + " " + x.SemiName,
                Id = x.Id,
                Href = x.Href
            }).ToList();

            foreach (var summaryOfPatient in patients)
            {
                var patient = summaryOfPatient;
                var patientInfo = _patientInfoRepository.Get(x => x.Id == patient.Id);
                summaryOfPatient.Disease = patientInfo.Diseases.Select(x => x.Name).ToList();
                var doctor = _doctorRepository.GetDoctorByPatientId(patient.Id);
                if (doctor == null)
                {
                    summaryOfPatient.NameDoctor = "Нет доктора";
                }
                else
                {
                    summaryOfPatient.NameDoctor = doctor.LastName + " " + doctor.FirstName + " " + doctor.SemiName;
                }
            }
            return patients;
        }

  
        public List<SummaryOfPatient> GetAllPatients()
        {
            var patients = _patientRepository.GetAll().Select(x => new SummaryOfPatient()
            {
                About = x.About,
                Address = x.Address,
                Age = DateTime.Now.Year - x.Brithday.Year,
                FullName = x.LastName + " " + x.FirstName + " " + x.SemiName,
                Id = x.Id,
                Href = x.Href,
            }).ToList();

            foreach (var patient in patients)
            {
                var summaryOfPatient = patient;
                var patientInfo = _patientInfoRepository.Get(x => x.Id == summaryOfPatient.Id);
                patient.Disease = patientInfo.Diseases.Select(x=>x.Name).ToList();
                var doctor = _doctorRepository.GetDoctorByPatientId(summaryOfPatient.Id);
                if (doctor == null)
                {
                    summaryOfPatient.NameDoctor = "Нет доктора";
                }
                else
                {
                    summaryOfPatient.NameDoctor = doctor.LastName + " " + doctor.FirstName + " " + doctor.SemiName;
                }
               
            }
            return patients;
        }

        public SummaryOfPatient GetSummaryPatient(String idpatient)
        {
            return GetAllPatients().FirstOrDefault(x => x.Id == idpatient);
        }


        public AddPatient GetPatient(String idpatient)
        {
            var patient = _patientRepository.GetById(idpatient);
            var disease = _patientInfoRepository.Get(x => x.Id == idpatient).Diseases.Select(x=>x.Name).ToList();
            return new AddPatient()
            {
                About = patient.About,
                Address = patient.Address,
                Birthday = patient.Brithday,
                Disease = disease,
                FirstName = patient.FirstName,
                LastName = patient.LastName,
                Password = "",
                RegisterDate = patient.DateRegister,
                SemiName = patient.SemiName,
                UserName = patient.UserName,
                Id=patient.Id,

Href=patient.Href

            };
        }

        public async Task<Boolean> CreatePatient(AddPatient patient)
        {
            var appUser = new ApplicationUser()
            {
                UserName = patient.UserName,
                About = patient.About,
                Brithday = patient.Birthday,
                FirstName = patient.FirstName,
                LastName = patient.LastName,
                SemiName = patient.SemiName,
                DateRegister = patient.RegisterDate,
                Href = patient.Href
            };
            var result = await _userManager.CreateAsync(appUser, patient.Password);
            if (result.Succeeded)
            {
                var user = _userRepository.Get(x=>x.UserName==patient.UserName);
                var addRoleResult = await _userManager.AddToRolesAsync(user.Id, new[] { "Patient" });
                if (addRoleResult.Succeeded)
                {
                    _patientInfoRepository.Add(new PatientInfo(){Patient = user,Diseases = patient.Disease.Select(x=>new Disease(){Name = x}).ToList()});
                }
                _unitOfWork.Commit();

            }
            return true;
        }

        public void UpdatePatient(AddPatient patient)
        {
            var applicationUser = _patientRepository.GetById(patient.Id);
            applicationUser.About = patient.About;

            applicationUser.Address = patient.Address;
            applicationUser.Brithday = patient.Birthday;
            applicationUser.DateRegister = patient.RegisterDate;
            applicationUser.FirstName = patient.FirstName;
            applicationUser.LastName = patient.LastName;
            applicationUser.SemiName = patient.SemiName;
            applicationUser.IsMale = patient.isMale;
            applicationUser.UserName = patient.UserName;
            applicationUser.Href = patient.Href;
            var diseases = _diseasesRepository.GetMany(x => x.UserInfo.Id == patient.Id);

           

            var removeDisase = diseases.Where(x => !patient.Disease.Contains(x.Name)).ToList();

            foreach (var removeDisas in removeDisase)
            {
                _diseasesRepository.Delete(removeDisas);
            }


            var patientInfo = _patientInfoRepository.Get(x => x.Id == patient.Id);
            foreach (var di in patient.Disease.Except(diseases.Select(x => x.Name)))
            {
                if (patientInfo.Diseases == null)
                {
                    patientInfo.Diseases = new List<Disease>();
                }
                patientInfo.Diseases.Add(new Disease() { Name = di });
            }


            _patientInfoRepository.Update(patientInfo);
            _doctorRepository.Update(applicationUser);
            _unitOfWork.Commit();
            if (!String.IsNullOrEmpty(patient.Password))
            {
                var provider = new DpapiDataProtectionProvider("Sample");
                _userManager.UserTokenProvider = new DataProtectorTokenProvider<ApplicationUser>(
    provider.Create("EmailConfirmation"));
                string resetToken = _userManager.GeneratePasswordResetToken(patient.Id);
                _userManager.ResetPassword(patient.Id, resetToken, patient.Password);

            }



        }

        public void RemovePatient(string id)
        {





            var user = _userManager.FindById(id);



            
            _patientInfoRepository.Delete(x => x.Id == id);
             _unitOfWork.Commit();
            
            var rolesForUser =  _userManager.GetRoles(id);
            if (rolesForUser.Any())
            {
                foreach (var item in rolesForUser.ToList())
                {
                    // item should be the name of the role
                    _userManager.RemoveFromRole(id, item);
                }
            }
            _userManager.Delete(user);

        }
    }
}
