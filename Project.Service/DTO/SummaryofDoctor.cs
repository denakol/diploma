﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Service.DTO
{
    public class SummaryOfDoctor
    {
        public String FullName { get; set; }
        public Int32 Age { get; set; }
        public String Address { get; set; }
        public String About { get; set; }
        public List<UserInfo> Patients { get; set; }
        public String Education { get; set; }

        public String Href { get; set; }
        public String Id { get; set; }
    }


    public class UserInfo
    {
        public String Href { get; set; }
        public String FullName { get; set; }

        public String Id { get; set; }
    }
}
