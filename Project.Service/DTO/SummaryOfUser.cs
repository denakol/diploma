﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Service.DTO
{
    public class SummaryOfPatient
    {

        public String Id { get; set; }
        public String FullName { get; set; }
        public Int32 Age { get; set; }

        public String Address { get; set; }

        public String About { get; set; }

        public String NameDoctor { get; set; }

        public String Href { get; set; }

        public StatePatient State { get; set; }

        public DateTime DateLastData { get; set; }

        public List<String> Disease { get; set; } 
    }
}
