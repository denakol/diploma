﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Service.DTO
{
    public class DoctorPatientInfo
    {
        public String Id { get; set; }

        public String FullName { get; set; }

        public String Href { get; set; }

        public IEnumerable<PatientItem> Patients { get; set; }
    }

    public class PatientItem
    {
        public string Id { get; set; }
        public string Href { get; set; }

        public String FullName { get; set; }

        public String Address { get; set; }
    }
}
