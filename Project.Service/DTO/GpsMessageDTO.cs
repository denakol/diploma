﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Service.DTO
{
    public class GpsMessageDTO
    {
        public Int32 Id { get; set; }

        public Double Latitude { get; set; }
        public Double Longitude { get; set; }
        public Double Speed { get; set; }
        public DateTime DateTime { get; set; }
    }
}
