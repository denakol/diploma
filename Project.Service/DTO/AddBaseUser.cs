﻿using System;

namespace Project.Service.DTO
{
    public class AddBaseUser
    {
        public String Id { get; set; }
        public String FirstName { get; set; }
        public String LastName { get; set; }
        public String SemiName { get; set; }
        
        public DateTime Birthday { get; set; }

        public DateTime RegisterDate { get; set; }

        public String About { get; set; }

        public String Password { get; set; }

        public String UserName { get; set; }

        public String Address { get; set; }

        public bool isMale { get; set; }

       public String Href { get; set; }
    }
}