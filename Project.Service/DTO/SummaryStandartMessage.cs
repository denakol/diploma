﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Service.DTO
{
    public class SummaryStandartMessage
    {

       public IEnumerable<Int32> HeartRate { get; set; }
       public IEnumerable<Int32> LastExtend { get; set; }

       public IEnumerable<DateTime> DateTimes { get; set; }
       public DateTime DateTime { get; set; } 
    }


    
}
