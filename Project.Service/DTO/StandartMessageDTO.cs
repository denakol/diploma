﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Service.DTO
{
    public class StandartMessageDTO
    {
        public Int32 Id { get; set; }

        public Int32 HeartRate { get; set; }

        public Int32[] Extend { get; set; }
    }
}
