﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using Project.Service.DTO;

namespace Project.Service.IService
{
    public interface IPatientService
    {
        List<SummaryOfPatient> GetAllPatients();
        List<SummaryOfPatient> GetPatientByDoctor(String idDoctor);

        AddPatient GetPatient(String idpatient);
        SummaryOfPatient GetSummaryPatient(String idpatient);
        Task<Boolean> CreatePatient(AddPatient patient);
        void UpdatePatient(AddPatient patient);
        void RemovePatient(string id);
    }
}
