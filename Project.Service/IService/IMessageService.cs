﻿using System.Collections.Generic;
using Project.Model.Models;
using Project.Service.DTO;

namespace Project.Service.IService
{
    public interface IMessageService
    {
        void LogMessage(string message);
        List<Message> GetAll();


        void CreateStandartMessage(string id, StandartMessageDTO value);
        void CreateGpsMessage(string id, GpsMessageDTO value);
        IEnumerable<GpsMessageDTO> GetAllGpsMessageForUser(string userId);
        IEnumerable<StandartMessageDTO> GetAllStandartMessageForUser(string userId);
        IEnumerable<GpsMessageDTO> GetGpsMessageForUserSummaryPage(string userId);
        SummaryStandartMessage GetStandartMessageForUserSummaryPage(string userId);
    }
}
