﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Project.Service.DTO;

namespace Project.Service.IService
{
    public interface IDoctorPatientService
    {
        IEnumerable<DoctorPatientInfo> GetAll();
        void Update(DoctorPatientInfo value);
    }
}
