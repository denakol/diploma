﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using Project.Service.DTO;

namespace Project.Service.IService
{
    public interface IInstructionService
    {

        IEnumerable<InstructionModel> GetByUser(String userId);
        void Archive(int id);
        void CreateInstruction(InstructionModel value);
    }
}
