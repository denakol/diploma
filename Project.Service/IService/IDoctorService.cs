﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using Project.Service.DTO;

namespace Project.Service.IService
{
    public interface IDoctorService
    {

        List<SummaryOfDoctor> GetAllDoctors();
        AddDoctor GetDoctor(String idDoctor);
        Boolean CreateDoctor(AddDoctor doctor);
        SummaryOfDoctor GetSummaryDoctor(String idDoctor);
        void UpdateDoctor(AddDoctor doctor);
        void RemoveDoctor(string id);
    }
}
