﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Model.Models
{
    public class DoctorPatient
    {

        [Key]
        public Int32 Id { get; set; }
        public String DoctorId { get; set; }


        public String PatientId { get; set; }


         [ForeignKey("DoctorId")]
        public ApplicationUser Doctor { get; set; }

         [ForeignKey("PatientId")]
        public ApplicationUser Patient { get; set; }
    }
}
