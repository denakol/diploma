﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.CompilerServices;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace Project.Model.Models
{
    public class ApplicationUser : IdentityUser
    {

        public String FirstName { get; set; }
        public String LastName { get; set; }
        public String SemiName { get; set; }

        public DateTime Brithday { get; set; }

        public DateTime DateRegister { get; set; }

        public String About { get; set; }
        public String Address { get; set; }

        public String Href { get; set; }
        public Boolean IsMale { get; set; }
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Обратите внимание, что authenticationType должен совпадать с типом, определенным в CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Здесь добавьте утверждения пользователя
            return userIdentity;
        }


        public virtual DoctorInfo DoctorInfo { get; set; }


        public virtual PatientInfo PatientInfo { get; set; }

        [InverseProperty("Doctor")]
        public virtual ICollection<DoctorPatient> Patients { get; set; }

         [InverseProperty("Patient")]
        public virtual ICollection<DoctorPatient> Doctor { get; set; }

    }
}
