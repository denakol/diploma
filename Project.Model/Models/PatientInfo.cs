﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Model.Models
{
    public class PatientInfo
    {

       [Key, ForeignKey("Patient")]
        public String Id { get; set; }
        public virtual ICollection<Disease> Diseases { get; set; }

        public virtual ICollection<Instruction> Instructions { get; set; }
        public virtual ICollection<GpsMessage> GpsMessages { get; set; }
        public virtual ICollection<StandartMessage> StandartMessages { get; set; }

        public virtual ApplicationUser Patient { get; set; }
    }
}
