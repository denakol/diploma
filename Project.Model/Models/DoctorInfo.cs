﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Model.Models
{
    public class DoctorInfo
    {

        [Key,ForeignKey("Doctor")]
        public String  Id { get; set; }

        public String Education { get; set; }


        public virtual ICollection<Instruction> Instructions { get; set; }


        public virtual ApplicationUser Doctor { get; set; }
    }
}
