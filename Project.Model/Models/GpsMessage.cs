﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Model.Models
{
    public class GpsMessage
    {
        [Key]
        public Int32 Id { get; set; }

        public Double Latitude { get; set; }
        public Double Longitude { get; set; }
        public Double Speed { get; set; }

        public DateTime DateTime { get; set; }

        public String  PatientId { get; set; }
        public PatientInfo Patient { get; set; }
    }
}
