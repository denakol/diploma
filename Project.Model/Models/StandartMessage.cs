﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Model.Models
{
     public  class StandartMessage
     {


         [Key]
         public Int32 Id { get; set; }

         public String PatientId { get; set; }
         public PatientInfo Patient { get; set; }

         public Int32 HeartRate { get; set; }

         public DateTime DateTime { get; set; }
          public string InternalData { get; set; }
         
         
         
         [NotMapped]
         public double[] Data
         {
             get
             {
                 return Array.ConvertAll(InternalData.Split(','), Int32.Parse).Select(x=>Convert.ToDouble(x)).ToArray();
             }
             set
             {
                 InternalData = String.Join(";", value.Select(p => p.ToString()).ToArray());
             }
         }
       
     }
}
