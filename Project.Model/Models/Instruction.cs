﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Model.Models
{
    public class Instruction
    {
        [Key]
        public Int32 Id { get; set; }

        public String Title { get; set; }
        public String Description { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }

        public virtual PatientInfo UserInfo { get; set; }

        public virtual DoctorInfo DoctorInfo { get; set; }

        [ForeignKey("UserInfo")]
        public String PatientId { get; set; }

        [ForeignKey("DoctorInfo")]
        public String DoctorId { get; set; }
    }
}
